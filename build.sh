#!/bin/sh
set -e

cd 00\ Core
delta_plugin convert ncgdmw.yaml
cd ../test
delta_plugin convert 'ncgd base stats test.yaml'
