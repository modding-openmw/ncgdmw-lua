# NCGDMW Lua Edition

A "Natural Grow" leveling mod where your attributes and level grow automatically as your skills increase, and your skills will also decay over time (optional).

**OpenMW 0.49 or newer is required!**

A changelog is available in the [CHANGELOG.md](CHANGELOG.md) file, and an FAQ in the [FAQ.md](FAQ.md) file.

#### Features

* Your level and attributes grow as your skills grow
    * No more levelup menu and manual attribute increases, it just happens seamlessly now
* Uncap skills beyond 100 (200 by default)
* Uncap attributes beyond 100 (200 by default)
* Optional and configurable skill decay over time (off by default)
    * Decay is a way to make late-game players feel less overpowered - try it!
* Configurable skill and attribute growth settings (slow growth attribute, vanilla skill growth by default)
    * Skill progression in particular has a variety of options
* Optionally block skill increases from books (off by default)
* Optionally carry over excess skill progress after a level increase (on by default)
* Optional state-based HP (off by default)
* Configurable base and per-level HP ratio
    * Additionally: configurable per-level HP gain
* Using multi-school spells progresses all relevant skills proportionally to the theoretical cost of each school effects
* Magicka-based skill progression (on by default)
* Magicka refund for cast spells (off by default, configurable values)
* In-game stats menu for viewing level, attributes, skills and their decay progress, and more!
    * Bind it to a key via the in-game script settings menu: ESC >> Options >> Scripts >> NCGDMW Lua Edition
* Improved base gain for multiple skills and their different use types

#### How It Works

This mod blocks vanilla levelup progress and instead does work under the hood to calculate its own.

##### Level Calculation

The player's level is derived by adding up all skills and performing calculations on them.
If you're interested in the specific implementation details, please check out the `updatePlayerStats()` function in the `00 Core/scripts/NCGDMW/player.lua` file.

The mod operates using [the skill progression interface](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/interface_skill_progression.html). This means:

* The mod only operates when it needs to versus every frame
* It can and does take over skill progress handling at the engine level
* Uncapping and progress modifications are done more seamlessly as a result
* **NCGDMW Lua Edition is explicitly not compatible with other mods that try to affect skill progress using this interface!**

##### Attributes

Attribute values are calculated based on the value of governed skills.
There is a spreadsheet included (`00 Core/docs/ncgd profile.ods`) that breaks down the relations between attributes and skills as well as their weights.
That file also allows the automatic computation of attributes growth, player level, and level progress by filling in a player profile:
Starting skills and attributes, and skill levels up.

##### Skills

Skill growth works as normal but with modified growth rates depending on what version of this mod you're using (please see [full installation details below](#installation)).

As skills gain levels, Vanilla Morrowind increases the skill gain progress requirement to reach next level, but it's generally not enough, players get overpowered too quickly.

Two NCGD settings allow skill gains to be even more reduced:
- Skill progression factor: Reduce all skill gains by a constant factor (e.g. all skill gains are halved)
- Skill progression squared level factor: Reduce all skill gains by a factor exponentially based on skill level

In the spreadsheet included (`00 Core/docs/ncgd profile.ods`) you can play with these two settings and see how it affects skill gains.

Please note: If you for whatever reason need to use the console to raise a skill, please do so using [the skill progression interface](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/interface_skill_progression.html##(SkillProgression).skillLevelUp) like so:

1. Press \` to bring down the console
1. Type `luap` and press enter
1. Type `I.SkillProgression.skillLevelUp("Skill ID here")` and press enter
1. Repeat the previous step as needed
1. Press \` to hide the console

Allowed skill IDs: acrobatics, alchemy, alteration, armorer, athletics, axe, block, bluntweapon, conjuration, destruction,
enchant, handtohand, heavyarmor, illusion, lightarmor, longblade, marksman, mediumarmor, mercantile, mysticism,
restoration, security, shortblade, sneak, spear, speechcraft, unarmored

##### Health

Player HP are based on:
- Attributes: Endurance, strength and willpower.
- Player level: More HP after each level up.
- HP growth settings: Which define base HP value as well as how much HP are gain on each level up.
- State-Based HP setting: Which adjusts HP values based on the current state of your HP, also factoring fortifications and all other base stat changes.

Player starting HP are based on the following formula: `endurance * 4/7 + strength * 2/7 + willpower * 1/7`.
This value can be reduced using the "Base and per level HP ratio" setting.

On each level up, a fraction of that formula is added to the player HP, depending on the "Per level HP gain" setting:
- High: 10% of the formula
- Low: 5% of the formula

##### Decay

The decay mechanic can be a nice way to balance the game and stave off becoming too powerful too quickly. Some things to keep in mind about this mechanic:

* Decay affects skills, which also has an impact on attributes as their growth part is based on skill values.
* Decay is represented as a percent value that increases slowly over time (check the value in the stats menu).
* Decay depends on the skill level. High level skills decay faster.
* A skill cannot decay past one half of its previous maximum value. So if your Acrobatics skill was at 50, it can't decay past 25.
* Regardless of a skill's max value, it will not decay below 15.
* When you level up a skill, half of decay progress is removed.
* When you use a skill, a small amount of decay progress is removed. Also, other skills of same specialization get a tiny reduction by synergy.
* When resting in a bed, decay does not progress
* When resting without a bed, only half of the time passed is added to decay progress
* When waiting, decay progresses normally
* When using a transport, only half of the time passed is added to decay progress
* When training a skill, the decay progress of that skill is cancelled and only half of the time passed is added to other skills of same specialization

##### Block Skill Increases From Books

If enabled, this feature does just what it says: You will no longer receive skill increases from reading books.

##### Carry Over Excess Skill Progress

With the vanilla Morrowind leveling, when a skill is increased, if there is any excess progress it is simply lost.
When this feature is enabled, that excess progress is carried over toward the next increase.

##### Magicka-Based Skill Progression

This feature will grant skill progress for magical skills based on the amount of magicka that a given spell costs (versus the flat amount in the vanilla game).

##### Magicka Refund

With the magicka refund feature, some amount of magicka will be returned after casting a spell (configurable via the script settings menu).

##### Multi-school spells

When using a multi-school spell, NCGDMW will distribute the skill gain (XP) among all implied schools, proportionally to the theoretic individual cost of their respective effects.

#### Credits

This is a port of [Greywander's "Natural Character Growth and Decay - MW" mod](https://www.nexusmods.com/morrowind/mods/44967) for the OpenMW-Lua API.
We've since extended it by adding new features and altering existing ones based on experience with what works and feedback.

##### Original concept, MWScript edition author

Greywander

##### Lua edition authors

Lua authors: EvilEye, johnnyhostile, Mehdi Yousfi-Monod

Original MBSP+Uncapper author: [Phoenixwarrior7](https://www.nexusmods.com/morrowind/users/6708039)

NCGDMW Patches: Alvazir

##### Localization

PT_BR: Karolz

DE: Atahualpa

ES: drumvee

FR: [Rob from Rob's Red Hot Spot](https://www.youtube.com/channel/UCue7D0dm2SilBhbVe3SB75g)

PL: emcek

RU: [dmbaturin](https://baturin.org/)

SV: Lysol

#### Installation

If you're following a mod list on Modding-OpenMW.com then please refer to the list-specific instructions provided there.

```
data="C:\games\OpenMWMods\Leveling\ncgdmw-lua\00 Core"

content=ncgdmw.omwaddon
content=ncgdmw.omwscripts
```

No extra or alternate plugins are needed for any known total conversions.

#### The Stats Menu

By default, it has no keybinding, you can set that in the script settings menu: ESC >> Options >> Scripts >> NCGDMW Lua Edition >> Stats Menu Key

This menu will show information about all stats managed by this mod as well as other useful details.
If displayed while the game is paused (e.g., you have some other UI up like inventory and use the stats menu key) you can mouse over fields and see a useful tooltip.

#### Saved Games Upgrade

If you're upgrading from NCGDMW 3.2 or above, there is nothing to do, you can safely play with your existing saved games.

If you are upgrading from NCGDMW 3.1 or below, your player's profile will be upgraded automatically when loading your saved game in order to:
- recalculate the attributes values
- clear the old active spells (abilities, curses)
- properly apply profile active spells (from birthsign and race)

NCGDMW will try to preserve your previous changes to attributes that are external to NCGD, for instance a strength gain of +2 given during a quest with a naked Nord.

You can check the detected base attributes and skills changes in the new stats menu.
For attributes, it includes external changes and spells fortifications from abilities.

If the external changes are broken after the automatic upgrade, you can manually reset your profile by following the "Reset Player's Profile" procedure described in next section.

If you remember the legit external attributes changes, you can manually apply them after the ResetStats command.

Example for the strength attribute and the +2 missing bonus:
1. Open your player's profile window
1. Read and remember the attribute value you want to change, for instance strength is 37
1. Open the console
1. Type `player->setstrength 39`

NCGD will now remember that bonus and preserve it during skill levels up.

#### Player's Profile Reset

If something went wrong with your player's attributes, skills, health or active spells, you can manually reset your profile by proceeding as follows:
1. Press \` to bring down the console
1. Type `luap` and press enter
1. Type `I.NCGDMW.ResetStats()` and press enter
1. Type `exit()` to quit the Lua mode
1. Exit the console, wait 2 seconds, and you should see NCGD popups informing you of the attributes updates and NCGDMW init finished
1. Check your player's profile and the stats menu to be sure everything went well

#### Compatibility With Other Mods

Some mods out in the wild may produce unexpected behavior when used with NCGDMW Lua Edition.
Please see the "Mod Conflict" tag [on the project issue tracker](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/?label_name=Mod%20Conflict) for more information about known, unsolved cases of this.

#### Known Compatibility Issues

Aside from what's stated in the [`FAQ.md`](FAQ.md) document about compatibility, there are some known issues with unknown causes out there.

#### How To Test This Mod

A simple plugin has been created that tests handling of external (non-NCGD) changes to stats.
This can be found in the included `test` folder, add that as you could any other mod and load a test character to observe:

* Stats handled properly with buffs and debuffs
* External changes to attributes and health are seen in the stats menu

1. [Run OpenMW in test mode](https://modding-openmw.com/tips/performance/#how-to-test)
1. Press \` to bring down the console
1. Type `luap` and press enter
1. Type `I.SkillProgression.skillLevelUp("armorer")` and press enter
1. Press the up arrow key, then enter to repeat that command
1. Do this a dozen or more times to see attributes recalculate and the player's level increase

#### Help Localize NCGDMW

Do you speak a language that's not yet offered and want to contribute a new localization? Follow these steps:

1. Download a release zip from [this URL](https://modding-openmw.gitlab.io/ncgdmw-lua/)
1. Open the `00 Core/l10n/NCGDMW/en.yaml` file with your favorite text editor ([Notepad++](https://notepad-plus-plus.org/) is recommended for Windows)
1. Update each line (the quoted part after the `:`) as desired
1. Save the file with the name `<CODE>.yaml`, where `<CODE>` is the [language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) for the localization you're adding
1. Commit your change to git and [open a merge request](https://gitlab.com/modding-openmw/ncgdmw-lua/-/merge_requests/new), or simply email the file to `ncgdmw@modding-openmw.com` with `New NCGD Localization` as the subject

#### Web

* [Project Home](https://modding-openmw.gitlab.io/ncgdmw-lua/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/53136)
* [Source on GitLab](https://gitlab.com/modding-openmw/ncgdmw-lua)

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/new) for bug reports or feature requests
* Email: `ncgdmw at modding-openmw dot com`

#### Planned Features

* Optional level up song and animation
* View decay progress by mousing over a skill
* [Request a feature!](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/new)
