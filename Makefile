clean:
	rm -f *.zip *.sha*sum.txt version.txt
	find . -type f -name "*.omwaddon" -delete

build:
	./build.sh

pkg: build
	./pkg.sh

web-clean:
	cd web && rm -rf build site/*.md sha256sums*

web: web-clean
	cd web && ./build.sh

web-debug: web-clean
	cd web && ./build.sh --debug

web-verbose: web-clean
	cd web && ./build.sh --verbose

clean-all: clean web-clean

local-web:
	cd web && python3 -m http.server -d build
