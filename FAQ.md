## NCGDMW Lua Edition Frequently Asked Questions

#### Q) How can I make my mod compatible with NCGDMW Lua Edition?

A) Generally speaking, NCGDMW Lua Edition is compatible with most things out of
   the box.

   If you're writing a Lua mod, it can interoperate with NCGDMW through its interface.
   Refer to the interface code in `player.lua` where the interface is defined: Search for `local interface = {`.
   Possible interactions are:

* Get or set values for attribute, skill, skill progress and skill affected attributes
* Get level progress
* Get no decay time
* Reset player stats (only for OpenMW 0.49)

#### Q) How does this mod work? Why does it do what it does?

A) Please see [the README](/readme/#how-it-works) for a high-level explanation.

#### Q) How does this compare/relate to the original version on Nexus Mods?

A) This is a continuation of [the original NCGDMW by Greywander](https://www.nexusmods.com/morrowind/mods/44967),
   which is no longer supported and doesn't work with modern versions of
   OpenMW.

   That one was last updated in 2018, and since then changes were made to
   OpenMW which caused breakage in the mod since it unfortunately relied on
   "features" that were actually implementation inaccuracies (aka bugs).

   Atahualpa and I kept the mod going in the face of breakage (you can find
   that version [here](https://modding-openmw.com/mods/natural-character-growth-and-decay-mw/)),
   but during the development of OpenMW 0.48 it got to the point where there
   was no easy way to keep the mod working. Changes in that version make magic
   work more like Morrowind.exe, but unfortunately break assumptions made by
   this mod.

   Thanks to the efforts of many individuals that worked on OpenMW-Lua, I was
   able to port the vast majority of the mod's functionality over to a new
   version that's written with Lua. This version lacks the mastery mechanic
   which allowed skills and attributes to level up past 100, but it sports
   all new features including:

* Level up and decay progress are now visible via a dedicated NCGD stats menu
* The mod itself is made with far fewer lines of code (thanks to Lua features)
* A Lua interface is provided for other mods to interoperate with this one (see
  the next question below for more information)
* It will also work with any other mod or script that happens to alter
  attributes outside of this mod's control (by applying those changes on top of
  our own calculations)
* ... And more to be added in the future!

#### Q) My level progress isn't increasing, when will I level up?

A) At this time, mods can't change the base game user interface, and NCGD
   zeroes out the vanilla level progress.

   The NCGD Stats Menu will show your level and decay progress if that feature
   is enabled. To use the menu, set it to a key using the mod settings menu.
